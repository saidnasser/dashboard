$(function () {
    $('.nav-item.navigation-item > a.nav-link').map(function () {
        console.log($(this))
        $(this).on('click', function (event) {
            // console.log('clicked');
            // console.log($(this).children('.child-nav-item').css('display'));
            $(this).parent('.nav-item.navigation-item').siblings('.nav-item.navigation-item').map(function () {
                if ($(this).children('.child-nav-item').css('display') === 'block') {
                    $(this).removeClass('active')
                    $(this).children('.child-nav-item').animate({
                        // height: "100%"
                        height: "toggle"
                    }, 500)
                }
            })
            // let that = this;
            /** -------------------------------------------------------------- */
            // $(this).parent('.nav-item.navigation-item').toggleClass('active')
            $(this).siblings('.child-nav-item').animate({
                // height: "100%"
                height: "toggle"
            }, 500, function () {
                // console.log($(this).children('.child-nav-item').css('display'));
            }.bind(this))
        })
    })








    console.log('document ready');
    console.log('top height : ', $('.top-nav').height());
    $('.side-bar').css({
        top: $('.top-nav').height(),
        height: window.innerHeight - $('.top-nav').height()
    })
    $('.overlay').css({
        top: $('.top-nav').height(),
        height: window.innerHeight - $('.top-nav').height()
    })
    if (window.innerWidth <= 991) {
        $('.side-bar').addClass('minimal')
        $('#main-content').css({
            // marginLeft: `70px`
        })
    } else {
        $('.side-bar').removeClass('minimal')
        $('.side-bar').css({ display: 'block' })
        $('#main-content').css({
            marginLeft: `${(window.innerWidth / 5) - 3}px`
        })
    }
    /** ---------------- toggle sidebar ------------------------------ */
    $('.side-bar').addClass('minimal-bar-show')
    // $('#main-content').css('margin-left', `${window.innerWidth / 5}px`)
    $('.nav-toggler button').on('click', function (event) {
        if (window.innerWidth > 991) {
            // var width = $('.side-bar').hasClass('.minimal') ? 'toggle' : '70px';
            // console.log(width);
            if ($('.side-bar').hasClass('minimal')) {
                $('.side-bar').animate({
                    width: `${window.innerWidth / 5}px`
                }, 500, () => {
                    resize()
                    $('.side-bar').css({ display: 'block' })
                    console.log('resize done');
                })
                $('#main-content').animate({
                    marginLeft: `${window.innerWidth / 5}px`
                }, 500)
                $('.side-bar').removeClass('minimal')
            } else {
                $('.side-bar').animate({
                    width: '70px'
                }, 500, () => {
                    resize()
                    $('.side-bar').css({ display: 'block' })
                    console.log('resize done');
                })
                $('#main-content').animate({
                    marginLeft: '70px'
                }, 500)
                $('.side-bar').addClass('minimal')
            }
        } else {
            var marginLeft = '0px'
            if (window.innerWidth <= 425) {
                marginLeft = '0px'
                $('.side-bar, .overlay').animate({
                    width: 'toggle'
                }, 500, () => {
                    // resize()
                    console.log('resize done');
                })
            } else {
                marginLeft = '70px'
                $('.side-bar').animate({
                    width: 'toggle'
                }, 500, () => {
                    // resize()
                    console.log('resize done');
                })
            }
            $('.side-bar').toggleClass('minimal-bar-show')
            
            if ($('.side-bar').hasClass('minimal-bar-show')) {
                $('#main-content').animate({
                    marginLeft: `0px`
                }, 500, () => {
                    // resize()
                    console.log('resize done');
                })
            } else {
                $('#main-content').animate({
                    marginLeft: marginLeft
                }, 500, () => {
                    // resize()
                    console.log('resize done');
                })
                if (marginLeft === '0px') {
                    $('.overlay').animate({
                        // width: `toggle`
                    }, 500)
                }
            }
        }
        // resize charts
        // resize()
    })
    /** -------------------------------------------------------------- */
    /** -------------------------------------------------------------- */
    // window.addEventListener('load', function (event) {

    // resize charts
    // resize()
    // })
    window.addEventListener('resize', function (event) {
        if (window.innerWidth <= 991) {
            $('.side-bar').addClass('minimal')
            if ($('.side-bar').css('display') === 'none') {
                $('#main-content').css({
                    marginLeft: `0px`
                })
            } else {
                $('#main-content').css({
                    marginLeft: `70px`
                })
                $('.side-bar').css({
                    width: `70px`
                })

            }
        } else {
            $('.side-bar').removeClass('minimal')
            if ($('.side-bar').css('display') === 'none') {
                $('#main-content').css({
                    marginLeft: `0px`
                })
            } else {
                $('#main-content').css({
                    marginLeft: `${(window.innerWidth / 5) - 3}px`
                })
                $('.side-bar').css({
                    width: `${window.innerWidth / 5}px`,
                    display: 'block'
                })

            }
            // $('#main-content').css({
            //     marginLeft: `${(window.innerWidth / 5)-3}px`
            // })
        }

        // resize charts
        resize()


    })
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    /** --------------------------------------------------------------------------------------------------------- */
    var costPerUnit,
        costPerUnitOption,
        devices,
        devicesOption,
        orders,
        ordersOption,
        performance,
        performanceOption,
        alternativePerformance,
        alternativePerformanceOption,
        weeklyHours,
        weeklyHoursOption,
        project,
        projectOption;

    window.addEventListener('load', function (event) {
        setTimeout(() => {


            if (document.querySelector('#cost-per-unit')) {
                costPerUnit = echarts.init(document.querySelector('#cost-per-unit'));
                costPerUnitOption = {
                    type: 'line',
                    xAxis: {
                        data: [0, 10, 20, 30, 40, 50, 60],
                        show: false
                    },
                    yAxis: {
                        type: 'value',
                        show: false,
                        max: 65
                    },
                    series: [{
                        data: [0, 15, 10, 25, 30, 15, 40, 50, 80, 60, 55, 65],
                        type: 'line',
                        smooth: true
                    }]
                };
                costPerUnit.setOption(costPerUnitOption)
            }
            /** ---------------------------------------------------------------- */
            /** ---------------------------------------------------------------- */
            if (document.querySelector('#devices')) {
                devices = echarts.init(document.querySelector('#devices'));
                devicesOption = {



                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}: {c} ({d}%)"
                    },
                    legend: {
                        orient: 'horizontal',
                        x: 'center',
                        y: 'bottom',
                        data: ['Desktop', 'Tablet', 'Mobile']
                    },

                    series: [
                        {
                            name: 'Devices',
                            type: 'pie',
                            radius: ['55%', '70%'],
                            avoidLabelOverlap: false,
                            label: {
                                normal: {
                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    show: true,
                                    textStyle: {
                                        fontSize: '30',
                                        fontWeight: 'bold'
                                    }
                                }
                            },
                            labelLine: {
                                normal: {
                                    show: false
                                }
                            },
                            data: [
                                { value: 335, name: 'Desktop' },
                                { value: 310, name: 'Tablet' },
                                { value: 234, name: 'Mobile' }
                            ]
                        }
                    ]

                };
                devices.setOption(devicesOption)
            }

            /** ---------------------------------------------------------------- */
            /** -------------------------- orders chart ------------------------ */
            if (document.querySelector('#orders')) {
                orders = echarts.init(document.querySelector('#orders'));
                ordersOption = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow',
                            label: {
                                show: true
                            }
                        }
                    },
                    // toolbox: {
                    //     show: true,
                    //     feature: {
                    //         mark: { show: true },
                    //         dataView: { show: true, readOnly: false },
                    //         magicType: { show: true, type: ['line', 'bar'] },
                    //         restore: { show: true },
                    //         saveAsImage: { show: true }
                    //     }
                    // },
                    calculable: true,
                    legend: {
                        data: ['Growth', 'Sales', 'Affiliate'],
                        itemGap: 5
                    },
                    grid: {
                        top: '12%',
                        left: '1%',
                        right: '10%',
                        // width: '100%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                            type: 'category',
                            axisLine: {
                                show: false
                            },
                            axisTick: {
                                show: false
                            },
                            data: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            // name : 'Budget (million USD)',
                            // show: false,
                            axisLine: {
                                show: false
                            },
                            axisTick: {
                                show: false
                            },
                            splitLine: {
                                lineStyle: {
                                    type: "dashed"
                                }
                            },
                            axisLabel: {
                                formatter: function (a) {
                                    a = +a;
                                    return isFinite(a)
                                        ? "$" + echarts.format.addCommas(+a) + "k"
                                        : '';
                                }
                            }
                        }
                    ],
                    // dataZoom: [
                    //     {
                    //         show: true,
                    //         start: 94,
                    //         end: 100
                    //     },
                    //     {
                    //         type: 'inside',
                    //         start: 94,
                    //         end: 100
                    //     },
                    //     {
                    //         show: true,
                    //         yAxisIndex: 0,
                    //         filterMode: 'empty',
                    //         width: 30,
                    //         height: '80%',
                    //         showDataShadow: false,
                    //         left: '93%'
                    //     }
                    // ],
                    series: [
                        {
                            name: 'Sales',
                            type: 'bar',
                            barWidth: 7.5,
                            itemStyle: {
                                barBorderRadius: 40
                            },
                            data: [0, 10, 5, 15, 10, 20, 15, 25, 20, 30, 25, 40]
                        },
                        {
                            name: 'Affiliate',
                            type: 'bar',
                            barWidth: 7.5,
                            itemStyle: {
                                barBorderRadius: 40
                            },
                            data: [5, 20, 30, 22, 17, 10, 18, 26, 28, 26, 20, 32]
                        }
                    ]
                };

                orders.setOption(ordersOption);
            }
            /** ---------------------------------------------------------------- */
            /** ------------------- performance chart -------------------------- */
            if (document.querySelector('#performance')) {
                performance = echarts.init(document.querySelector('#performance'));
                performanceOption = {
                    legend: {
                        data: ['All', 'Direct', 'Organic'],
                        align: 'left',
                        x: 'right'
                    },
                    // toolbox: {
                    //     // y: 'bottom',
                    //     feature: {
                    //         magicType: {
                    //             type: ['stack', 'tiled']
                    //         },
                    //         dataView: {},
                    //         saveAsImage: {
                    //             pixelRatio: 2
                    //         }
                    //     }
                    // },
                    tooltip: {},
                    xAxis: {
                        type: 'category',
                        data: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                        silent: false,
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        }
                    },
                    yAxis: {
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        splitLine: {
                            lineStyle: {
                                type: "dashed"
                            }
                        },
                        axisLabel: {
                            formatter: function (a) {
                                a = +a;
                                return isFinite(a)
                                    ? "$" + echarts.format.addCommas(+a) + "k"
                                    : '';
                            }
                        }
                    },
                    series: [{
                        name: 'Direct',
                        type: 'line',
                        data: [0, 15, 7, 10, 20, 15, 20, 30, 25, 30, 25, 40],
                        smooth: true,
                        animationDelay: function (idx) {
                            return idx * 10;
                        }
                    }, {
                        name: 'Organic',
                        type: 'line',
                        data: [0, 10, 5, 15, 10, 20, 15, 25, 20, 30, 25, 30],
                        smooth: true,
                        animationDelay: function (idx) {
                            return idx * 10 + 100;
                        }
                    }, {
                        name: 'All',
                        type: 'line',
                        data: [5, 17, 12, 18, 15, 20, 15, 30, 35, 33, 35, 40],
                        smooth: true,
                        animationDelay: function (idx) {
                            return idx * 10 + 100;
                        }
                    }],
                    animationEasing: 'elasticOut',
                    animationDelayUpdate: function (idx) {
                        return idx * 5;
                    }
                };
                performance.setOption(performanceOption);
            }
            /** ---------------------------------------------------------------- */
            /** ----------------- alternative performance chart ---------------- */
            if (document.querySelector('#alternative-performance')) {
                alternativePerformance = echarts.init(document.querySelector('#alternative-performance'));
                alternativePerformanceOption = {
                    tooltip: {},
                    xAxis: {
                        type: 'category',
                        data: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                        silent: false,
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            color: '#8297b4',
                            margin: 20
                        }
                    },
                    yAxis: {
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        splitLine: {
                            lineStyle: {
                                type: "dashed",
                                color: '#4d688a'
                            }
                        },
                        axisLabel: {
                            color: '#8297b4',
                            margin: 20,
                            formatter: function (a) {
                                a = +a;
                                return isFinite(a)
                                    ? "$" + echarts.format.addCommas(+a) + "k"
                                    : '';
                            }
                        }
                    },
                    series: [{
                        name: 'Direct',
                        type: 'line',
                        data: [0, 10, 5, 15, 10, 20, 15, 25, 20, 30, 25, 40],
                        smooth: true,
                        itemStyle: {
                            color: '#2c7be5'
                        },
                        animationDelay: function (idx) {
                            return idx * 10;
                        }
                    }],
                    animationEasing: 'elasticOut',
                    animationDelayUpdate: function (idx) {
                        return idx * 5;
                    }
                };

                alternativePerformance.setOption(alternativePerformanceOption);
                // buttons
                document.querySelector('#earnings').addEventListener('click', function (e) {
                    $('#sessions').removeClass('active')
                    $('#bounce').removeClass('active')
                    $(this).addClass('active')
                    alternativePerformance.setOption({
                        ...alternativePerformanceOption,
                        series: [
                            {
                                ...alternativePerformanceOption.series,
                                data: [0, 10, 5, 15, 10, 20, 15, 25, 20, 30, 25, 40]
                            }
                        ]
                    });
                })
                document.querySelector('#sessions').addEventListener('click', function (e) {
                    $('#earnings').removeClass('active')
                    $('#bounce').removeClass('active')
                    $(this).addClass('active')
                    alternativePerformance.setOption({
                        ...alternativePerformanceOption,
                        series: [
                            {
                                ...alternativePerformanceOption.series,
                                data: [0, 15, 7, 35, 20, 30, 25, 40, 35, 30, 25, 50]
                            }
                        ]
                    });
                })
                document.querySelector('#bounce').addEventListener('click', function (e) {
                    $('#sessions').removeClass('active')
                    $('#earnings').removeClass('active')
                    $(this).addClass('active')
                    alternativePerformance.setOption({
                        ...alternativePerformanceOption,
                        series: [
                            {
                                ...alternativePerformanceOption.series,
                                data: [0, 15, 7, 20, 18, 15, 20, 40, 25, 30, 25, 50]
                            }
                        ]
                    });
                })
            }
            /** ---------------------------------------------------------------- */
            /** ------------------------ weekly hours -------------------------- */
            if (document.querySelector('#weekly-hours')) {
                weeklyHours = echarts.init(document.querySelector('#weekly-hours'));
                weeklyHoursOption = {
                    tooltip: {},
                    xAxis: {
                        type: 'category',
                        data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                        silent: false,
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            color: '#8297b4',
                            margin: 20
                        }
                    },
                    yAxis: {
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        splitLine: {
                            lineStyle: {
                                type: "dashed",
                                color: '#283e59'
                            }
                        },
                        axisLabel: {
                            color: '#8297b4',
                            margin: 20,
                            formatter: function (a) {
                                a = +a;
                                return isFinite(a)
                                    ? "$" + echarts.format.addCommas(+a) + "k"
                                    : '';
                            }
                        }
                    },
                    series: [{
                        name: 'Direct',
                        type: 'bar',
                        data: [21, 12, 28, 15, 5, 12, 17, 2],
                        smooth: true,
                        barWidth: 10,
                        itemStyle: {
                            color: '#2c7be5',
                            barBorderRadius: 40
                        },
                        animationDelay: function (idx) {
                            return idx * 10;
                        }
                    }],
                    animationEasing: 'elasticOut',
                    animationDelayUpdate: function (idx) {
                        return idx * 5;
                    }
                };

                weeklyHours.setOption(weeklyHoursOption);
            }

            /** ---------------------------------------------------------------- */

            /** ----------------- project chart ---------------- */
            if (document.querySelector('#project')) {
                project = echarts.init(document.querySelector('#project'));
                projectOption = {
                    tooltip: {},
                    xAxis: {
                        type: 'category',
                        data: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                        silent: false,
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            color: '#8297b4',
                            margin: 20
                        }
                    },
                    yAxis: {
                        axisLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        splitLine: {
                            lineStyle: {
                                type: "dashed",
                                color: '#4d688a'
                            }
                        },
                        axisLabel: {
                            color: '#8297b4',
                            margin: 20,
                            formatter: function (a) {
                                a = +a;
                                return isFinite(a)
                                    ? "$" + echarts.format.addCommas(+a) + "k"
                                    : '';
                            }
                        }
                    },
                    series: [{
                        name: 'Direct',
                        type: 'line',
                        data: [0, 10, 5, 15, 10, 20, 15, 25, 20, 30, 25, 40],
                        smooth: true,
                        itemStyle: {
                            color: '#2c7be5'
                        },
                        animationDelay: function (idx) {
                            return idx * 10;
                        }
                    }],
                    animationEasing: 'elasticOut',
                    animationDelayUpdate: function (idx) {
                        return idx * 5;
                    }
                };

                project.setOption(projectOption);
                // buttons
                document.querySelector('#earned').addEventListener('click', function (e) {
                    project.setOption({
                        ...projectOption,
                        series: [
                            {
                                ...projectOption.series,
                                data: [0, 10, 5, 15, 10, 20, 15, 25, 20, 30, 25, 40]
                            }
                        ]
                    });
                })
                document.querySelector('#hours-worked').addEventListener('click', function (e) {
                    project.setOption({
                        ...projectOption,
                        series: [
                            {
                                ...projectOption.series,
                                data: [0, 15, 7, 35, 20, 30, 25, 40, 35, 30, 25, 50]
                            }
                        ]
                    });
                })
            }


        }, 100);

    })
    /** ---------------------------------------------------------------- */
    function resize() {
        if (document.querySelector('#cost-per-unit')) {
            // costPerUnit.clear()
            // costPerUnit = echarts.init(document.querySelector('#cost-per-unit'));
            // costPerUnit.setOption(costPerUnitOption)
            // costPerUnit.resize({ width: $('#cost-per-unit').width()})
            costPerUnit.resize()
        }
        if (document.querySelector('#devices')) {
            // devices.clear()
            // devices = echarts.init(document.querySelector('#devices'));
            // devices.setOption(devicesOption)
            // devices.resize({ width: $('#devices').width()})
            devices.resize()
        }
        if (document.querySelector('#orders')) {
            // orders.clear()
            // orders = echarts.init(document.querySelector('#orders'));
            // orders.setOption(ordersOption)
            // orders.resize({ width: $('#orders').width()})
            orders.resize()
        }
        if (document.querySelector('#performance')) {
            // performance.clear()
            // performance = echarts.init(document.querySelector('#performance'));
            // performance.setOption(performanceOption)
            // performance.resize({ width: $('#performance').width()})
            performance.resize()
        }
        if (document.querySelector('#alternative-performance')) {
            // alternativePerformance.clear()
            // alternativePerformance = echarts.init(document.querySelector('#alternative-performance'));
            // alternativePerformance.setOption(alternativePerformanceOption)
            // alternativePerformance.resize({ width: $('#alternative-performance').width()})
            alternativePerformance.resize()
        }
        if (document.querySelector('#weekly-hours')) {
            // weeklyHours.clear()
            // weeklyHours = echarts.init(document.querySelector('#weekly-hours'));
            // weeklyHours.setOption(weeklyHoursOption)
            // weeklyHours.resize({ width: $('#weekly-hours').width()})
            weeklyHours.resize()
        }
        if (document.querySelector('#project')) {
            // project.clear()
            // project = echarts.init(document.querySelector('#project'));
            // project.setOption(projectOption)
            // project.resize({ width: $('#project').width()})
            project.resize()
        }
    }





    /** ---------------------------------------------------------------- */
    $('a.avatar').map(function () {
        $(this).on('hover', function (event) {
            $(this).tooltip('show')
        })
    })
    /** ---------------------------------------------------------------- */

    /** ------------------------ initialize editor ---------------------------------------- */
    if (document.querySelector('#editor')) {
        var quill = new Quill('#editor', {
            theme: 'snow'
        });
    }
    /** ---------------------------------------------------------------- */
    /** ---------------------------------------------------------------- */
    if (document.querySelector('#add-new-members')) {
        $('#add-new-members').select2({
            // theme: 'snow'
        });
    }
    /** ---------------------------------------------------------------- */
    /** ---------------------------------------------------------------- */
    if (document.querySelector('#dropzone')) {
        console.log('=================');
        // var myDropzone = new Dropzone("div#dropzone", { url: "/file/post" });
        $('3dropzone').dropzone({ url: "/file/post" })
    }
    /** ---------------------------------------------------------------- */
    /** --------------------- select all  ------------------------------ */
    $('#ordersSelectAll').on('change', function (event) {
        console.log($(this).prop('checked'));
        if ($(this).prop('checked')) {
            $('.custom-control-input[type="checkbox"]').map(function () {
                $(this).prop('checked', true)
            })
        } else {
            $('.custom-control-input[type="checkbox"]').map(function () {
                $(this).prop('checked', false)
            })

        }
    })
    /** ---------------------------------------------------------------- */

})